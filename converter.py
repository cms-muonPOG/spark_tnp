from pathlib import Path
import getpass
import uproot
import pyarrow as pa
import pyarrow.parquet as pqt
from tqdm import tqdm
import os
import gc

from registry import registry
from dataset_allowed_definitions import get_allowed_sub_eras


def pandas_convert(paths, outDir, tree="muon/Events", step_size="100 MB"):
    pqwriter = None
    file_paths = [str(Path(path).resolve()) for path in paths]
    
    # open multiple files and iterate over chunks
    df_iter = uproot.iterate({path: tree for path in file_paths}, step_size=step_size, library='pd')
    
    for i, df_pd in tqdm(enumerate(df_iter)):
        
        # output name for current iteration
        output_path = outDir / f"output_{i}.parquet"
        
        # convert pandas dataframe to parquet file
        table = pa.Table.from_pandas(df_pd)
        with pqt.ParquetWriter(output_path, table.schema) as pqwriter:
            pqwriter.write_table(table)

        del table
        gc.collect()

    return


def run_convert(particle, resonance, era, dataTier, subEra, customDir='', baseDir='', use_pog_space=False):
    '''
    Converts a directory of root files into parquet
    '''

    if baseDir == '':
        if use_pog_space is False:
            uname = getpass.getuser()
            eospath = '/eos/user/' + uname[0] + '/' + uname
            basepath = eospath
        else:
            basepath = '/eos/cms/store/group/phys_muon/TagAndProbe'
        inDir = Path(basepath) / 'root' / customDir / particle / resonance / era / dataTier / subEra
        outDir = inDir
    else:
        inDir = Path(baseDir)
        outDir = Path(baseDir)

    fnames = list(inDir.glob('*.root'))
    outName = outDir / 'tnp.parquet'

    os.makedirs(outName, exist_ok=True)

    print(f'>>>>>>>>> Path to input root files: {inDir}')
    print(f'>>>>>>>>> Path to output parquet files: {outName}')
    
    print(f'>>>>>>>>> Number of files to process: {len(fnames)}')
    if len(fnames) == 0:
        print('>>>>>>>>> Error! No ROOT files found to convert with desired options.')
        print('>>>>>>>>> Exiting...')
        return
    print(f'>>>>>>>>> First file: {fnames[0]}')

    pandas_convert(fnames, outDir=outName, tree="muon/Events", step_size="500 MB")
    
    return


def run_all(particle, resonance, era, dataTier, subEra=None, customDir='', baseDir='', use_pog_space=False, use_local=False):

    if subEra is not None:
        subEras = [subEra]
    else:
        subEras = get_allowed_sub_eras(resonance, era)
        # subEras by default includes whole era too, so remove for convert
        subEras.remove(era)

    for subEra in subEras:
        print('\n>>>>>>>>> Converting:', particle, resonance, era, subEra)
        run_convert(particle, resonance, era, dataTier, subEra, customDir, baseDir, use_pog_space)

    return
